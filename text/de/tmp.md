## Eine Abweichung vom Natürlichen, die in ihrer Entstehung davon abhängt, wie wir denken

Zuerst möchte ich über den Ausgangspunkt meines Denkens sprechen.
Das Leben ist der Sinn des Lebens. Das bedeutet, dass das Leben gegeben ist, es ist nichts, was wir wählen. Wir müssen leben, um zu überleben, so lange wir können. Wir können den Wunsch haben, es möge anders sein. Wir können uns wünschen, dass wir einer Menge Unbehaglichkeiten und Schmerzen, Elend und anderem aus dem Wege gehen könnten, aber wir haben nicht gewählt, auf die Welt zu kommen, sondern das haben wir sowieso getan und da gibt es keine Wahl, nicht dazusein, nicht zu leben.
Es sagt jemand, dass wir Selbstmord begehen könnten. Gewiss, dass könnten wir, aber eigentlich doch nicht. Es ist etwas, was wir uns vorgaukeln, aber es ist im völligen Widerstreit mit dem primären Sinn des Lebens und dem Wert, den wir haben, indem wir da sind. Wir können nichts dazufügen oder abziehen von diesem Wert. Wir können etwas tun, damit wir unsere Selbständigkeit oder unser Leben verlieren, jedoch nicht unseren Wert. Dieser ist gegeben dadurch, dass es uns gibt.
Weshalb sage ich das? Ja, versetze ich meinen Fokus darauf, ob ich leben soll oder nicht, so wird alles andere unwichtig. Es wird wie ein Zwang andauernd zu ver­suchen, mich zu entscheiden, ob ich leben soll oder nicht, und weshalb ich in dem einen Fall weiterleben soll, oder was besser wäre daran, dass ich sterben würde. Da lande ich in Fragen, die mich nur unglücklich machen, die das Leben und meine psychische Gesundheit nur verwirren, die nur Chaos in mir verursachen und bewirken, dass alles unaus­stehlich und übel wird.
Es ist unmenschlich zu denken, dass wir über Leben und Tod entscheiden, und es zerstört alle Lebensqualität.
Als Menschen brauchen wir etwas, worüber wir eine Gewissheit haben. Ich habe eine tiefe Gewissheit darüber, dass der Sinn des Lebens das Leben selbst ist, und dass der Wert darin besteht, dass wir als einzigartige Menschen da sind, dass wir keine Kopien sind, sondern nur Originale. In meine Gedanken geht ein, dass mit jedem Menschen, dem ich begegne, mit jeder Begegnung mit einem Menschen, etwas Neues geschieht. Das beruht darauf, dass wir als Menschen uns seit dem letzten Mal geändert haben können oder wenn wir uns noch nie getroffen haben, dann ist es ganz neu.
Für viele Menschen ist es das, was sie in den Religionen finden. Durch den Glauben an Gott, in welcher Form auch immer, ist dieser Sinn des Lebens und der Wert gegeben, und wenn man sich darauf stützen kann, dann kann man alles im Leben angehen aus dieser Grund­geborgenheit.
Ich pflege nicht Stellung zu nehmen zu dem, was ist, bevor ich bin, und auch nicht zu dem, was danach ist. Früh genug werde ich es erfahren. Aber diese kurze Zeit auf der Erde will ich mich, so gut es geht, dem Begreifen des Lebens widmen und dazu habe ich Gelegenheit, wenn ich sicher bin darin, dass das Leben gegeben ist, unbedingt, und dass ich es zu leben habe auf eine so lebendige Weise, mit einer so guten Lebensqualität, wie es mir möglich ist.
Wenn du als Mensch dennoch nicht anders kannst als mit dem Leben auf Tod-oder-Leben-Ebene herumzudribbeln, dann handelt es sich um etwas ganz anderes, das bedeutet, dass du Furcht bekommen hast, einen Schrecken, in einem Schock gelandet bist, in einem Trauma, aus welchem du nicht herauskommst. Das heißt, dass du dich versteift hast und dich festklammerst, um etwas zu haben, an dem du dich festhalten kannst, um nicht in Chaos oder innerer Leere zu landen. Wenn wir meinen, nicht in Gemein­schaft zu sein, und meinen, nichts tun zu können, um aus diesem Zustand wieder herauszukommen, dann be­kommen wir oft starke Furcht und es wird innen dunkel und dann fangen wir oft an zu ringen mit dem Gedanken, uns selbst wegzunehmen.
Schritt Nummer eins ist, anzufangen zu sprechen, Worte anzuwenden, eine Sprache zu finden für das Unaus­gesprochene in uns drin. Es reicht also, zu wissen, dass man weiß, dass man das braucht.
„So gut wie möglich“ ist nichts Objektives, es ist erlebt, aber es geht darum, es gut zu haben, d.h. nicht nur glücklich zu sein und auf Wolken zu gehen, dann ist man natürlich glücklich, sondern relevant auf die Wirklichkeit zu reagieren, wie diese auch aussehen mag. So kann man eine Wahrnehmung davon bekommen, dass das Leben genau so ist, wie es sein soll. Gelingt es einem, dies hin und wieder zu tangieren, dann ist das Leben gut. Ich sammle solche Augenblicke und ich denke oft darüber nach, was im Wege steht, wenn ich keine solchen bekomme und dann ändere ich einiges, bis ich zufrieden bin. Das ist ein Spiel, ein Auskundschaften des Lebens, ein ständiges Experimentieren und das gibt dem Leben einen interessanten Inhalt.
Das geht nicht, wenn man sich anpasst. Woran man sich auch immer anpasst, man wird die Autonomie verlieren, wird die Kreativität, das aktive Handeln, das neugierige Schaffen, Ausforschen, das, was jedes Kind als Neu­ge­borenes mit sich bringt, verlieren. Da wird man in eine Falle gesteuert.
Ja, aber, sagt jemand, so ist doch das Leben. Wir müssen uns ja anpassen, um in dieser Gesellschaft, in dieser Zivilisation zu überleben. Wie würde es aussehen, wenn alle nur an ihrem eigenen Glück interessiert wären? Das würde nicht gehen. (Das würde ausgezeichnet gehen, aber es wurde in unserer Kultur nicht ausprobiert und deshalb glauben wir, dass es nicht geht.)
Hier liegt ein Majoritätsmissverständnis vor.
Wenn ich autonom bin, kann ich genau das tun, was andere von mir erbitten und wollen, und brauche mich dennoch nicht anzupassen.
Wenn jemand mich bittet, ein Glas Wasser zu holen, dann kann ich das tun, weil der andere es will und mich anpassen und erwarten, dadurch zufrieden zu werden, dass der andere danke sagt.
Oder ich kann dem, was der andere sagt, Trotz ent­gegenbringen und darauf pfeifen, Wasser zu holen, und sagen, das könne er oder sie gerade so gut selbst tun.
Ich kann auch den Fokus in mich selbst hinein­versetzen, mich fragen, ob ich einen Grund habe, das Wasser nicht zu holen, einsehen, dass ich einen solchen Grund nicht habe und das Wasser holen und zufrieden sein damit, schon bevor ich es getan habe.
Es macht einen fundamentalen Unterschied in mir, ob ich in Anpassung getreten bin, passiv oder trotzend, gegenüber der Aufforderung des anderen Menschen, oder ob ich mir die Aufforderung zu eigen gemacht habe und das tue, was ich tue, weil es zum Leben gehört, das zu tun, was andere wollen.
Wir sprechen viel zu wenig über diesen Unterschied, darüber eine Lage in uns selbst zu finden, wo wir gerne das Beste, was wir haben, hergeben und selber damit zufrieden sind, gänzlich ohne Belohnung. Wir tappen so oft in die Falle, dass wir meinen, andere könnten unsere Bedürfnisse zufriedenstellen, dass wir selbst nicht zufrie­den sein können mit dem, was wir anderen geben, ohne dass diese wiederum uns etwas geben. Das macht, dass wir in einer Abhängigkeit landen und dabei gelangen wir in eine Untergebenenlage und Anpassung anstelle von Autonomie.
Unsere ganze Wirtschaft baut auf diesem Missverständnis auf.
Glauben wir als Menschen, dass irgendetwas außerhalb von uns uns glücklich machen kann, dann werden wir ständig auf der Jagd nach diesem Etwas sein. Wir arbeiten, um später etwas zu bekommen, was uns glücklich macht. Wir verschaffen uns eine Ausbildung, Zeugnisse, um eine gut bezahlte Arbeit zu suchen, wir werden auf ein Danach warten, ”wenn ich nur den/die Richtige/n treffe, nur eine Arbeit finde, die ich haben will, nur das Geld habe, um mir ein Auto kaufen zu können, ein Haus, ein Boot usw. Wenn ich bloß alles, was einen Menschen glücklich macht, sammeln und es behalten kann, was mir einen hohen Status verleiht, dann werde ich zufrieden sein.” Oft täuschen wir uns selber, indem wir denken ”hätte ich nur größere Brüste... hätte ich bloß nicht eine so hässliche Nase... wäre ich nur nicht so dick...“ Die ganze Zeit werden wir dazu verleitet, zu glauben, Glück wäre etwas, was wir uns verdienen können. Das ist Anpassung. Auch wenn wir uns dafür entscheiden, das gerade Gegenteil von etwas zu tun, sind wir gleichermaßen von dieser Entscheidung gesteuert.
Dies ist eine Tretmühle, in die wir oft geraten und aus der wir nicht immer sehen, wie wir herauskommen können, besonders nicht, wenn wir unser ganzes Wohlergehen auf diesem zuwachsökonomischen Boden aufgebaut haben, wo alles darauf hinausgeht, dass wir auf ein „Danach“ warten, um gute Produzenten und gute Konsumenten zu werden. Außerdem ökonomisieren wir alles, alle Formen der Pflege, des Services, der Fürsorge und dann verstehen wir nicht, weshalb wir einsam, isoliert und unglücklich werden. Wir haben uns an das Geld angepasst. Geld steuert unser Denken, Handeln und Nichthandeln. Das ist so ge­wohnt geworden, dass wir es normal nennen, und glauben, es sei etwas Gutes. Es ist allgemein vorhanden = normal, aber es ist deshalb nicht auch das Natürliche und Gute. Gerade das ist es, worüber wir sprechen müssen, um zu verstehen, was die Alternative ist, was ”die andere Seite” ist.
Hier folgen einige Gedanken über die Alternative, über „die andere Seite”.
Wir müssen genau und unbequem sein darin, zu durch­denken, was wir in unserem Leben tun, um in der Nähe des Guten zu sein.

Ein Beispiel:
Es ist so leicht, das Geld steuern zu lassen. Wenn wir eine Arbeit haben und ausreichend verdienen, dann ent­scheiden wir uns dafür, ein Haus zu kaufen. Wir nehmen ein Darlehen auf, um es bezahlen zu können, und außer­dem vermindert sich dadurch die Steuer, wir haben nun Schulden und sitzen in einer Falle. Wir müssen arbeiten, sonst können wir das Darlehen nicht bezahlen. Das Ganze wird zu einer Anpassung und einer Verstrickung, anstatt zu der Freiheit, nach der es am Anfang aussah.

Ein anderes Beispiel:
Wir geben unseren Kindern Taschengeld. Anfänglich ohne Bedingungen, sie dürfen kaufen, was sie wollen, Süßigkeiten usw. Und dann, wenn sie sich daran gewöhnt haben, dann legen wir dem Geld Bedingungen bei. Zum einen müssen sie etwas tun, um es zu bekommen, z. B das Zimmer aufräumen oder abwaschen, zum anderen dürfen sie es nicht beliebig ausgeben für Süßigkeiten oder so, sondern sie müssen es sparen für etwas, was die Eltern bestimmen.
Dies ist eine effektive Art, unsere Kinder in das Gesetz der Ökonomisierung einzulullen. Du wirst verlockt, dich ans Geld zu hängen, Taschengeld zu bekommen, und dann bekommst du es nicht ohne die Anpassung an die Forderungen, die ich stelle. Dann darfst du es nicht disponieren wie du willst, sondern nur in dem Rahmen des Gutgeheißenen.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wie oft bist du in einer Form von Stress oder Irritation, wenn du jemanden völlig uner­wartet triffst und diese Person bewirkt, dass dein Zeitplan platzt?
Besonders, wenn es eine Person ist, die das Bedürfnis hat zu sprechen, sprechen, sprechen, um ein bisschen Gemeinschaft in der Einsamkeit zu bekommen, oder eine Person, die dich um etwas bittet, das deine Zeit beansprucht?
Ist das bei dir so, dann bist du an das Joch der Zeit an­gepasst, an den Druck, alles hinzukriegen, was du geplant hast.
Dann hast du dir ein Bild der Wirklichkeit geschaffen, das nicht mehr mit der Wirklichkeit übereinstimmt und du wirst darüber irritiert und möchtest die Wirklichkeit ändern, damit sie mit deinem erwarteten Bild über­einstimmt.
Kannst du dein Bild in die Luft werfen und die Zeit fahren lassen und alle Zeit der Welt herbeiholen, um hier und jetzt dazusein, die Begegnung das Wichtigste der Welt sein zu lassen und darin zu verweilen, um es gut zu haben?
Willst du das Leben selbst das Bild gestalten lassen oder wehrst du dich dagegen und folgst lieber deinem eigenen Zeitplan?
Bist du bereit, das zu tun, was du zu tun brauchst, um es in einer Situation, wo dein Bild von der Wirklichkeit über den Haufen geworfen wird, gut zu haben?
Wie denkst du über das Leben?
Was ist wichtiger im Leben, dass die Sachen getan werden oder Beziehungen?
Tüchtig zu sein oder eine interessante Be­geg­nung zu haben?
Andauernd werden wir vor die Frage gestellt, angepasst zu sein oder dahin zu wechseln, autonom zu sein, dem zu folgen, was das Leben gerade hier und jetzt anbietet. Gleichgültig, was wir tun, ist es wichtig, dass wir dahin finden, wo wir autonom sind, um uns in uns drin wohl zu fühlen. Das ist eine harte Arbeit, diese Bewusstheit zu finden, die es möglich macht, dass wir eine Wahl haben.
Wie denkt ein jeder über Ökonomie und Geld?
Sind sie wichtiger als Menschen?
Wählen wir Geld, oder uns dem Geld an­zupassen, z. B. wenn es gilt, zu Hause bei unseren Kindern zu sein, sich um unsere Großeltern zu kümmern, Beziehungen zu pflegen usw?
Oftmals fallen wir unüberlegt den Gesetzen der Öko­nomie zum Opfer. Wir sehen es als unmöglich an, etwas dagegen zu unternehmen. Falls wir nun die Kranken­versicherung verlieren, das Pensionsgeld usw, wenn wir auf eine andere Art als das gewöhnliche Lohnarbeiten leben, dann bekommen wir Furcht davor, arm und ausgestoßen zu werden.
Unser Steuer- und Leihsystem ist so ausgeklügelt auf­gebaut, damit wir in einer Anpassungsfalle sitzen, so dass die Kinder- und Elternfürsorge eine Lohnarbeit sein soll und sie unser System der Wachstumswirtschaft regeln soll.
Ich weiß, dass es schwierig ist, außerhalb dieses Rahmens zu denken, aber ich wünsche mir, dass wir gemeinsam darüber reflektieren können, wie wir in dieses Majori­tätsmissverständnis verstrickt sind.
Was wird in euch geweckt, wenn ich etwas ”entlarve”, das so unbequem ist und über das es so unmöglich zu sein scheint, über­haupt nachzudenken, geschweige denn et­was dagegen zu tun?

</div>

Es ist vielleicht wichtig, darüber zu sprechen, wie ein jeder darüber fühlt, dass Kinder in Tagesschulen gesteckt werden, dass die Schule aussieht, was sie mit Mobbing tut und weshalb wir uns damit abfinden ohne ein­zugreifen, dass ältere Menschen in ihren Altersheimen schlecht gepflegt werden und nicht am pulsierenden Leben um sie herum teilhaben dürfen, dass die Ent­wicklungsgehemmten auf irgendeine Weise vom ratio­nellen Arbeitsmarkt weg sollen, weil sie nicht wirt­schaftlich sind.
Wie können wir darüber als ein Phänomen sprechen, ohne Opfer zu werden, dass wir uns anpassen und glauben, Wirtschaft ist bedingungslos gegeben, vom Himmel ge­fallen und nichts, was wir beeinflussen kön­nen.
Wie können wir darüber sprechen, Zivil­courage und Kraft genug zu haben, uns zusammenzuschließen und wirklich gegen den Strom des Wahnsinns, der darin be­steht, dass wir in ein Warten auf ein späteres Glück eingelullt werden, zu gehen?
Ich nehme ein kleines Beispiel:
In Uruguay gab es in der Mitte des 19. Jahrhunderts Demokratie. Dann brach die Wirtschaft in den 1970er Jahren zusammen, eben eine solche Wirtschaft, wie wir sie haben. Dann entstand dort eine Diktatur und es wurde fürchterlich.
Was geschah, damit die Diktatur in eine Demokratie überging?
Ja das Volk entschied sich dafür, Tauschhandel zu betreiben, anstatt Geld zu verdienen und so flossen dem Staat keine Finanzen zu. Die Guerilla leerte alle Banken und versteckte das Geld. Man schloss sich in Gruppen von 20-40 Personen zusammen, so dass niemand un­bemerkt verschwinden konnte. Diejenigen, die in den Gefängnissen saßen, sprachen mit den Wachen, bis diese daran zweifelten, was für ein System sie aufrecht erhielten usw.
Das führte dazu, dass Wahlen abgehalten wurden, weil es nach ein paar Jahren kein Geld mehr gab, um die Polizei und die Armee zu bezahlen. Das ist Autonomie, dass man nicht Opfer ist für etwas, das man dadurch beein­flussen kann, dass man wählt, wie man sich verhalten will.
Ein anderes Beispiel ist, als die Frauen in Deutschland während der achtziger Jahre zum Streik gegen den Verkauf von Säuglingsnahrung aufriefen, weil die Hersteller be­stim­mt hatten, dass man der Säuglingsnahrung Konser­vierungsmittel beifügen wollte.
Die Produzenten führten die Konservierungsmittel ein, doch nach weniger als einem Monat traten sie an die Öffentlichkeit und teilten mit, dass sie sie wieder ent­fernen werden, da der Verkauf um 85% gesunken war und dies zum Rande des Ruins geführt hatte.
Dadurch verschwand der Zusatz von anderem als Roh­wahren und ist nicht wiedergekehrt.
Dass man sich dem zuwendet und das beeinflusst, was schädlich ist für einen selbst, für andere und für das Leben, z.B. das, was zur Politik gehört, zum Geld und Finanzierungssystem, zu der Weise, wie wir leben, woh­nen und dem, was wir als wichtig erachten im Leben, in Beziehungen usw.
Es gibt wie viel auch immer, worüber man sprechen kann im Zusammenhang mit Anpassung und wir können wirklich Spaß haben dabei, wenn wir entlarven, wofür wir Opfer sind und an was wir uns anpassen, damit spielerisch umzugehen, ja das ist ein nützliches Spiel.
## Das Joch der Anpassung ist schwer zu tragen

Anpassung – GESPRÄCH

Was ist das, dass so viele Menschen fühlen, dass es ihnen nicht gut geht, dass es ihnen schlecht geht, obwohl sie alles haben, was sie brauchen für ihren Lebensunterhalt, und mehr dazu? Sie haben jeden Tag zu essen, ja so viel, dass sie ständig gezwungen sind, einiges wegzuwählen, und sie haben so viele Kleider, dass sie gezwungen sind, aus der jeweiligen Situation, Lust und Gutdünken heraus zu wählen. Sie haben nicht nur ein Dach über dem Kopf, sondern außerdem teure Wohnhäuser, die sie in eine Lage versetzen, in der sie viel zu viel von ihrer Zeit hergeben müssen, nur allein fürs Wohnen.
Was ist es, das macht, dass wir uns oft in eine Situation versetzen, wo wir so viel Geld verdienen ”müssen” und wir gleichzeitig finden, dass wir dabei an Lebensqualität verlieren?
Das Joch der Anpassung ist schwer zu tragen.
In unserer Kultur wird Anpassung als eine Tugend an­gesehen. Es ist fein, sich anzupassen. Man erhält einen hohen Status, wenn man tüchtig, schnell und effektiv ist und tut, was andere sagen.
Als Erwachsener bedeutet es Status, bequem zu sein, es hin­zukriegen, dass andere für einen arbeiten, auf irgendeine Weise der Leiter zu sein und andere unter sich zu haben, die ”die Arbeit machen”, auch alle Bequemlichkeiten wie Auto, Spülmaschine usw. zu haben.
Kinder an Tagesschulen werden gerühmt, wenn sie ”lieb” sind, d.h. sich an das anpassen, was der Erwachsene will, so dass der Erwachsene die Mühe umgehen kann, den Fokus zu verschieben und zusammen mit dem Kind zu reflektieren darüber, was vernünftig wäre und Freude bereiten würde. Jene Kinder, die dadurch protestieren, dass sie trotzen, werden als „schwierig“ angesehen und da wird es für den Erwachsenen wichtig ”die Macht an sich zu reißen, laut und deutlich zu werden und Grenzen zu setzen”. Trotzige Kinder passen sich nur durch Zwang an.
In der Schule wird von den Kindern erwartet, dass sie still sitzen und ruhig sind und lernen, was der Lehrer will, damit der Lehrer sich tüchtig fühlen kann, d.h. Beweise für seine Tauglichkeit durch das Resultat der Kinder erhält. Schüler, die es nicht mehr schaffen sich anzupassen, schwänzen und machen Radau und damit konstatiert die Erwachsenenwelt, dass etwas beim Individuum nicht stimmt, d.h. bei dem Schüler, oder dass er oder sie auf dem Weg in die Pubertät ist.
Zu Hause ist es dieselbe Sache. Die Eltern wollen, dass die Kinder sich gut benehmen und tüchtig sind, so dass sie in Ruhe arbeiten und sich selbst verwirklichen können, und nur dann Freude an ihren Kindern haben dürfen und in Gesellschaft anderer Stolz auf sie sein können. D.h. die Kinder räumen ihre Zimmer auf, putzen, machen die Hausaufgaben, betreiben Sport und sind fröhlich.
Das führt dazu, dass wir aus einer Bewertung heraus leben, die besagt, dass wir nur taugen, wenn wir perfekt, tüchtig, schnell, effektiv, reich sind und von anderen wegen unserer Vortrefflichkeit bewundert werden. In der Schule ist der Beweis dafür ein gutes Zeugnis. Die Kriterien im Freundeskreis sind: ”in” zu sein, das ”rechte” Aussehen zu haben, coole Kleider zu tragen und mit einer Menge Abwertungen umgehen zu können, wie z.B. „Hure“, „Schwuler“ und anderes.
Sich an die Bewertungen von anderen anzupassen, führt zu Unfreiheit und das mögen wir überhaupt gar nicht. Wir werden krank an Körper und Seele und es geht uns schlecht. Lange Zeit versuchen wir, das Problem dadurch zu lösen, dass wir noch mehr von dem verrichten, was uns schadet, wie etwa uns zur Verfügung zu stellen, uns aufzuopfern, bis zur Selbstaufgabe, generös zu sein, usw.
Bei Jugendlichen, die es nicht schaffen, sich anzupassen, führt das oft dazu, dass sie sich von der Erwachsenenwelt distanzieren und eine Subkultur entwickeln, wo sie ver­suchen, eigene Alternativen zu finden. Sie zeigen ihren Protest, indem sie Drogen nehmen, vandalisieren, kriminell sind, Esstörungen entwickeln oder Trainingsfreaks werden usw.
Bei einem Erwachsenen führt das dazu, dass er ”gegen eine Wand rennt”, ausgebrannt wird, Fibromyalgie (Faser-Muskel-Schmerz) be­kommt, verschiedene Allergien oder andere Über­emp­findlichkeiten. Viele werden von psychischen Problemen befallen und einige versuchen, sich das Leben zu nehmen.
Der Mythos in unserer Zeit ist, dass alles, was geschieht, individuell bedingt ist, dass es auf unserer Konstitution beruht oder auf dem, was wir als Kind erlebt haben. Es ist richtig, dass man immer individuelle Ursachen finden kann dafür, dass ein Mensch leidet, aber es stimmt nicht, dass 60% der in Luxus aufgewachsenen Menschen geschä­digt sind. Höchstens 3% haben eigene individuelle Pro­bleme, die bewirken, dass sie solche Symptome haben. Die übrigen sind Opfer von Denkfehlern.
Das große Problem ist, dass viele Menschen nicht wissen, dass es etwas anderes gibt; eine andere Lage, eine andere Verhaltensweise, als die angepasste. Dass es etwas anderes gibt als das, was gegeben ist: entweder sich in die Dinge zu fügen oder gegen das zu trotzen, in das sich andere fügen. Das sind zwei Seiten der Anpassung und sie haben nichts gemeinsam mit einer gesunden, frei auto­nomen und geborgenen Art zu funktionieren.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />
Erkennst du darin etwas aus deinem Leben wieder?
Gibt es etwas, was dich daran hindert, du selbst zu sein?
Weißt du, wer du bist?
Wovon gehst du aus, wenn du an den Sinn des Lebens denkst?
Wie denkst du über deinen eigenen Wert und den von anderen?
</div>

Autonomie

Im Grunde gibt es keine Bewertungen oder Bedingungen, die uns steuern, sondern das, was steuert, ist der Wunsch zu überleben, der Wille zu existieren. Es ist ein Kampf, sich zu entwickeln schon in der Gebärmutter, ein Kampf gegen alle zersetzenden Kräfte - dass wir heute da sind, zeigt, dass es uns gelungen ist, alle Gefahren zu über­winden.
Wir sind neugierig, aktiv handelnd, kreativ und schöpferisch. Wir sind erforschend und gleichzeitig völlig hilflos - wir sind abhängig von einer Umgebung, die sich um uns kümmert. Ein kleines Kind kann anfangs nicht einmal selbst essen, sondern braucht, dass wir einen Saugreflex bei ihm hervorlocken.
Wenn ein Kind in einer Umgebung aufwächst, in der es zufriedene und fähige Menschen gibt, die sich ganz selbstverständlich um das Kind kümmern, ohne das Bedürfnis zu haben ”tüchtig zu sein” und Bestätigungen dafür zu bekommen, dann hat das Kind eine große Chance, frei zu sein.
Paradoxerweise ist das Kind völlig abhängig von seiner Umgebung, um überleben zu können und deshalb ist es für den Erwachsenen so verlockend, dieses Ab­hängig­keitsverhältnis auszunutzen, um sich selber als tüchtig zu erleben. Aber gleichzeitig ist das Kind fähig, vollkommen autonom, ganz auf seine eigene Weise zufrieden zu wer­den mit dem, was es bekommt, dadurch, dass es den Fokus auf sich selbst richtet und sich zufrieden fühlt, wenn die Grundbedürfnisse gedeckt werden.
Es gibt eine Vorstellung über uns Menschen, die mit der Wirklichkeit nicht übereinstimmt, die uns jedoch über lange Zeit geprägt hat. Diese, dass wir, falls wir nicht lieb gemacht werden, böse und gefährlich werden. Das kommt daher, dass wir ”arme Sünder sind, die im Schweiße ihres Angesichts ihr Brot verdienen müssen”. Das sind Vorstellungen, die in der Entwicklung der Menschheit lange Zeit mitgeschleift wurden und ihre Wurzeln in den Deutungen alter biblischer Schriften haben. Solche Ansichten über den Menschen haben zur Folge, dass wir der Auffassung sind, ein Kind in die Anpassung drücken zu müssen, damit es fromm wird.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wie steht es mit dir und deiner Auffassung über den Menschen, glaubst du, dass wir im Grunde böse sind und zum Guten erzogen werden müssen?
Oder glaubst du, dass wir im Grunde gut sind und deshalb keine Stopps und Grenzen brauchen?
Oder glaubst du, dass wir weder gut noch böse sind, sondern das eine oder das andere werden auf Grund von dem, was uns zu­stößt?
Wie denkst du über dich selbst, erlebst du dich als geprägt von der Erziehung, der du ausgesetzt warst?
Wenn du ein Kind hast und dieses Kind weigert sich, sich an das anzupassen, was du willst, wie kommst du damit zurecht?
Nimm dir einige Minuten und denke nach, mach gerne eine Runde und frage, was die anderen sagen, überdenke, wie du selber reagierst. Oft spiegelt das, welchen Dingen du selbst ausgesetzt warst.

</div>
# Zutrauen zu mir selbst

Ein anderer Zustand als Anpassung findet sich ein, wenn du in dich selbst gehst und die Freude findest. Dann entstehen ganz andere Gedanken.
Dann fühlst du dich frei und zufrieden, erhältst eine Menge Energie und fühlst, dass du alle Kraft der Welt hast. Dann gibt es die innere Geborgenheit, die macht, dass das Ursprüngliche zugänglich wird, Phantasie, Krea­tivität, Neugierde, Lust und vieles mehr. Das nenne ich Autonomie, ein Zustand, wo man sich selbst zufrieden stellt und wo das Leben genau so ist, wie es sein soll.
Wenn du jemals im Leben so gefühlt hast, dann weißt du, dass es so etwas gibt. Es ist da, wenn nichts anderes da ist und dabei ist es ein Anliegen, etwas mit dem zu tun, was im Weg steht, so dass man dann und wann einen Lichtstrahl davon erhält.
Wenn du niemals so gefühlt hast, wenn du nicht verstehst, wovon ich spreche, dann ist es höchste Zeit, dass du Hilfe dabei bekommst, eine Wahrnehmung von dem zu er­halten, was waltet, wenn nichts anderes waltet.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Setze dich bequem hin und sei da anwesend, wo du gerade bist.
Konstatiere, dass du in Sicherheit bist, dass nichts da ist, das dein Leben bedroht.
Falls etwas auftaucht, das sich in den Weg stellt, schreibe dann auf, welche Gedanken und Gefühle auftauchen und dich be­hin­dern.
Denke: ”Der Sinn des Lebens ist zu leben und ich lebe, ich bin da - hier und jetzt bin ich genau so, wie ich bin”.
Wenn das nicht geht, schreibe auf, was dich hindert.

</div>
# Opfer und Täter

Für viele ist es nicht einmal möglich, überhaupt still zu sitzen und sich dafür Zeit zu nehmen. Sie bekommen Angst, es wird zu schmerzlich, es fühlt sich an, als wenn sie sterben müssten oder verrückt würden, falls sie eine solche Übung machen würden. Für einige fühlt es sich nur lächerlich an und abgeschmackt und ”gibt nichts her“. Für andere ist es schockartig traumatisch und alle un­heim­lichen Erinnerungen halten Einzug in sie.
Alle, die eine innere Zufriedenheit erleben können wäh­rend einer solchen Übung, die können leicht mit Hilfe von primärer Gedankenarbeit jenes innere Gleichgewicht erlangen, das macht, dass das Leben so wird, wie es sein soll, voller Kraft und Energie, Engagement und allen nur erdenklichen Gefühlen.
Andere können Hilfe nötig haben, um mit einer Pri­märarbeit in Gang zu kommen, dadurch, dass jemand anderes sie dahin führt, so dass sie Kontakt bekommen mit dieser Dimension der Anwesenheit und Lebenskraft, die ja immerzu da ist.
Für diejenigen, die nicht einmal in die Nähe von diesem kommen können, die die ganze Zeit mit einer Menge von Bewertungen kämpfen und inneren Vorstellungen davon, nicht zu taugen, nicht das Recht zu leben zu haben, nicht glücklich zu sein, nicht spielen oder fröhlich sein zu dürfen, nicht erwachsen und nicht klein, die sind zu tief in der Anpassung stecken geblieben. Sie ahnen, dass sie einen weiten Weg mit harter Arbeit vor sich haben, um all das, was ihnen im Weg steht, wegzuschieben, alles das, was an Diktate angepasst ist, die oftmals vor dem Erwerb der Sprache, unverbal, sich der Persönlichkeit eingeprägt haben. Deshalb wählen diese meistens, in eine andere Richtung zu schauen.
Beziehungen zu anderen Menschen zu haben, ist das Wichtigste für unser Überleben (außer den Dingen, die man für seine Existenz braucht.)
Wir brauchen Intimität, Begegnung und Gemein­schaft. Dazu haben wir jederzeit die Möglichkeit, wenn wir einen anderen Menschen in unserer Nähe haben.
Wir wissen, wie es ist, in Begegnung zu sein. Das wird schon in der Gebärmutter angelegt, in der Beziehung von Mutter und Kind und dann nach der Geburt, in Beziehung zu den Nächsten, die die ganze Zeit um das Kind herum sind.
Wenn etwas geschehen ist, was dieses frühe Erlebnis von ”in Begegnung” zu sein aus der Bahn geworfen hat oder gestört hat, dann wird unsere Erinnerung der Intimität unzureichend sein, und dann werden wir es mit etwas anderem, z.B. mit Bewunderung, ersetzen.
Wir passen uns an andere an und werden tüchtig, stark, schnell, schmeichlerisch usw. Nicht, weil wir es sind, sondern damit andere uns gerne mögen und uns die Aufmerksamkeit und Bestätigung geben, die uns spüren lässt, dass wir jemand sind.
Das macht uns abhängig von anderen. Wir fangen an, Rollen anzunehmen, wir schaffen uns eine Identität, die uns dazu veranlasst, andere dazu zu bringen, uns zu geben, was wir brauchen.
Wenn es nicht klappt mit Bewunderung, dann können wir Macht ausüben. Wir werden unvoraussehbar, dro­hend, fordernd, abwertend, wir sehen zu, dass wir Informationen haben, die andere von uns abhängig machen, und wir streben danach, eine hierarchische Position zu bekommen, die andere dazu bringt, dass sie uns aufsuchen müssen und eine Beziehung zu uns haben müssen, damit ihr Leben funktioniert.
Wenn wir weder Bewunderung noch Macht bekommen können, können wir so beschwerlich und so pochend und fordernd sein, dass uns mit Verachtung begegnet wird.
Es gibt eine Anzahl von Gefühlsspielereien, die wir spielen können, in denen wir in verschiedene Rollenfiguren hüpfen und dadurch eine Menge Auf­merk­sam­keit und Gefühle erhalten, die machen, dass wir nicht untergehen, auch wenn wir keine positive Intimität hinkriegen.
Niemand überlebt ohne Aufmerksamkeit, auf die eine oder andere Weise sehen wir zu, dass wir sie erhalten.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Gibt es Gelegenheiten, wo du dich als ein Opfer erlebst und dich nicht unabhängig fühlen kannst von dem, was andere sagen oder tun?
Gibt es Gelegenheiten, wo du dich als Täter fühlst, wo du die Wahrheit sagen musst, kritisieren musst und anderen misstraust und dich berechtigt fühlst, auf andere los­zugehen?
Gibt es Gelegenheiten, wo du der Fähigkeit von anderen, ihre eigenen Probleme zu lösen, misstraust, so dass du eingreifst und die Person und die Situation rettest, wo du die Initiative ergreifst und eine Bedeutung be­kommst?
Gibt es Gelegenheiten, wo du in Beziehungen involviert bist und merkst, dass du in eine dieser Positionen (Opfer, Täter oder Retter) fällst, wo auch deine Position wechselt, und auch ein­ge­misch­te Personen ihre Position wech­seln, was zu einem Gefühlsspiel ausartet?

</div>
# Machtlosigkeit

Wenn man ein Kind ist und die Erwachsenen nicht autonom, sondern angepasst sind, dann wird man in das Gefühlsspiel der Erwachsenen mit hineingezogen. Man wird zur Spielfigur. Das Kind wird dann die Bedürfnisse des Erwachsenen nach Intimität auslösen und das be­wirkt, dass die natürlichen Bedürfnisse des Kindes nach Intimität und Hilfe zu einer Konkurrenz gegenüber den Bedürfnissen des Erwachsenen werden. Dann wird der Erwachsene das Kind als anstrengend, fordernd, als böse, als irritierend usw empfinden. Er wird es abweisen, es erziehen dadurch, dass er es schreien lässt, und machtlos dem Kinde das Notwendigste geben, jedoch ohne Intimität. Es geht vielleicht sogar so weit, dass der Erwachsene das Kind schlägt, um es zu unterdrücken und anzupassen, so dass es nicht so viel Aufmerksamkeit fordert.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wenn du Kinder hast, hast du dich jemals gegenüber den Bedürfnissen des Kindes ohn­­mächtig gefühlt?
Als du selbst Kind warst, kannst du dich erinnern, wie du die Reaktionen der Er­wachsenen auf dich erlebtest?
Als du Jugendliche/r warst, hattest du da Probleme mit deinem Selbstgefühl? Zeigte es sich in Form von z. B. Esstörungen, Leis­tungszwang und Schwänzen, oder frühem sexuellem Debüt, Drogen usw.?
Hast du dich einsam, wertlos und “außer­halb“ gefühlt? Hast an deinem eigenen Wert zu existieren gezweifelt?

</div>
# Eine Geissel

Anpassung hat so viele Gesichter, dass es schwer ist, alle Konsequenzen aufzuzählen, die sie mit sich bringen kann. Aber, was wir erleben, wenn wir angepasst sind, ist ein Gefühl von Abhängigkeit von etwas oder jemandem außerhalb von uns selbst, dass wir machtlos sind und dass Verantwortung schwer zu tragen ist und nur zu Unfreiheit führt. Oft taucht ein Gefühl auf, dass man revoltieren möchte, sich herausschlagen, frei werden, Grenzen spreng­en, doch es strandet oft in einem lahmen Protest, oft darin, dass man krank wird oder es einem übel geht.
Hinter der Anpassung liegt Misstrauen in unsere eigene Fähigkeit, unsere eigenen Bedürfnisse zufriedenstellen zu können. Wir meinen, abhängig und angepasst sein zu müssen, um das zu bekommen, was wir für unser Überleben brauchen. Wir werden gefügig und gesell­schaftlich angepasst.
Hinter der Anpassung liegt auch Furcht davor, „außer­halb“ zu sein, nicht mit dabei zu sein, abgewiesen zu werden und in Konflikten zu landen, die uns ausstoßen und einsam machen. Es wird auch schwierig, zu wählen. Alles, was nicht von Anfang an kontrollierbar ist, weckt schon im Voraus Angst.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

In Bezug auf andere Menschen fürchtest dich davor, ...
... abgewiesen zu werden.
... mit anderen in Konflikt zu geraten.
... Verantwortung auf dich zu nehmen und Kritik für die Konsequenzen zu bekommen.
... nicht genug tüchtig zu sein oder zu tüchtig zu sein.
... dass andere von dir abhängig werden.
... dass jemand einen Haken bei dir findet, so dass du ”entlarvt”werden kannst.
... dass du in einer Abhängigkeit landest.

</div>
## Das Gegebene

Gemeinschaft ist das Natürliche für uns. Sie wird jedoch oft aus der Bahn geworfen aufgrund der Ängste vor den Konsequenzen, sowohl praktisch als gefühlsmäßig. Deshalb wollen wir Gemeinschaft schaffen, aber das geht nicht. Gemeinschaft kann man nicht schaffen, die gibt es, wenn wir nur das wegnehmen, was ihr im Wege steht. Das ist es, was sich so schwierig ausnimmt. Zum Teil zu sehen, was der Gemeinschaft im Wege steht, aber auch in wirklicher Gemeinschaft zu stehen, wenn man alle Hin­der­nisse entfernt hat. Das ist oft schmerzlich und wir fürchten uns oft vor dem Schmerz, der der Intimität vorausgeht. Oft umgehen wir die Intimität und gehen über in eine Sexualisierung, werden davon angezogen und verschieben den Fokus auf das Geschlechtliche, anstelle von menschlicher Begegnung.
Es ist nichts falsch an Verliebtheit und Sexualität, aber wenn sie eine Art der Flucht vor Intimität sind, dann werden sie leer und sinnlos. Es wird kränkend und abwertend, sowohl für einen selbst, als auch für den anderen. Das Resultat ist, dass wir leicht in einem symbio­tischen Verhältnis landen, wo wir uns in die gegenseitigen Gefühle und Gedanken verwickeln. Wir geraten in ein Gefühlsspiel anstelle von Intimität und werden leicht eifersüchtig, flehend und beherrschend. Dies ist auch ein Ausdruck für die Schadenswirkungen der Anpassung.
Viele verschiedene Aspekte der Gefühlsspiele, in die wir oft geraten, wenn wir eigentlich Intimität erleben sollten, sind ausgiebig beschrieben in einem Buch von Erik Berne, das ”Games People Play” heißt.
Die Spiele haben oft einen treffenden Titel, der macht, dass wir leicht verschiedene Spiele, in denen wir mitten drin stecken, entlarven können. Manchmal sind wir es selbst, die mit den Spielen anfangen, manchmal fallen wir in diejenigen anderer hinein. In beiden Fällen müssen wir beachten, dass es an Intimität mangelt und dass dies die Hauptursache dafür ist, dass wir uns verstricken.
”Weshalb tust du nicht..?.“
“Ja aber...”
Stellen wir uns vor, wir würden jemanden treffen, die gerne Hilfe und Unterstützung haben möchte, jemanden, der uns um Rat fragt. Wir fühlen uns geehrt und strengen uns an, richtig gute Ratschläge zu geben. Aber alle Ratschläge werden zurückgewiesen mit einer Er­klärung dafür, weshalb es nicht geht, sie auszuführen. Anfänglich hören wir mit Verständnis zu, doch nach einer Weile fangen wir an, leicht irritiert zu werden. Was so gut anfing damit, dass wir in Anspruch genommen wurden und Vertrauen erhielten, endet damit, dass wir uns als Opfer erleben und uns machtlos fühlen, weil nichts von dem, womit wir beitragen, taugt.
Das Resultat wird, dass beide sich enttäuscht fühlen, derjenige, der um Rat gebeten hat, fühlt, dass es sowieso vorbei ist, dass es nicht geht, etwas aus der Situation zu machen, und derjenige, der so bereitwillig Ratschläge ausgeteilt hat, fühlt sich ausgenützt und missbraucht.
Als Folge wird sich jeder im Wechselspiel der Rollen des Retters, Täters oder Opfers wiedererkennen und darin sein wankendes Selbstwertgefühl und seine bisherigen Werte vom Leben und den Menschen bestätigt fühlen.
“Ich muss nur kurz...”
Jedesmal, wenn man die Gelegenheit hat, etwas Roman­tisches, Freundliches, Spaßiges, Genussvolles zu unter­nehmen, kommt der andere auf etwas, was unbedingt zuerst erledigt werden muss. Derjenige, der eingeladen hatte, etwas zu unternehmen, wartet und wartet und ..., bis er oder sie dermaßen irritiert und frustriert ist, dass dasjenige, was so reizend war, verschwindet und deshalb egal ist. Beide werden die Situation als misslungen emp­finden und werden eine Spur von negativer Bestätigung in der Enttäuschung erleben.
Erwachsene tun oft so mit Kindern, um der Intimität mit dem Kind aus dem Wege zu gehen. Das führt dazu, dass das Kind sich nicht okay fühlt, fühlt, dass etwas an ihm nicht stimmt, und so fängt es an, sich anzupassen, um nicht zu sehr „daneben“ zu sein.
Aber es hilft doch nicht, weil der Erwachsene trotzdem immer intuitiv einen Grund finden wird, der Intimität auszuweichen.
”Schau, wozu du mich gebracht hast!”
Wenn einem etwas missglückt ist oder etwas geschieht, was nicht berechnet war, etwas, was kritisiert werden kann, dann meint man oft, jemand anderes habe geschal­tet und gewaltet, der andere sei an dem Ganzen schuld. Dieses Spiel kann auch sein ”wenn du nicht wärest, dann...”
Wenn man es selbst nicht hinkriegt, in der Intimität zu verbleiben, die entsteht, wenn einem etwas misslingt oder man einen Fehler begeht, wenn man nicht in dem Schmerz verweilen kann und die Konsequenzen und die Verantwortung für die Situation auf sich nehmen kann, dann wird man ein Opfer für seine eigenen Fehlschritte werden. Das wird so unerträglich, dass man ein Gefühls­spiel startet, das andere mit in den eigenen Konflikt zieht, nur um von der Intimität fortzukommen.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Kennst du etwas davon wieder?
Wenn ja, wie denkst du dann über Intimität, anstelle von Gefühlsspiel?
Kannst du irgendeine Möglichkeit sehen, dies zu durchbrechen und in eine Begegnung mit demjenigen zu treten, mit dem du im Gefühlsspiel bist oder der mit dir im Ge­fühls­spiel ist?
Kannst du darauf pfeifen, Recht zu haben?
Kannst du auf Rache und Revanche pfeifen?

</div>
# Ein gefühlsmässiger Zustand

Spüre nach und mach dir Gedanken über „Begegnung“, „Intimität“, „Anwesenheit“, „Gemeinschaft“, „offen und verletzlich sein“, „Schmerz“ und „Zufriedenheit“. Fühle nach, wie all das zusammenhängt, wie es un­voraussehbar ist und dass es positiv angsterfüllt sein kann. Fühle, wie die Freude in dir aufsprudelt und sich manch­mal wie ein unausstehbarer Schmerz anfühlen kann, fühle, wie du eine innere Geborgenheit bekommst, die bewirkt, dass ein Signal dir anzeigt, dass du ”ausladen” kannst, dass alte gelagerte Gefühle und Erinnerungen hochkommen und machen, dass du weinst und schreist, rasend bist und frierst in einem wilden Durcheinander. Du kennst dich selbst nicht mehr wieder, oder du erlebst es, wie wenn du nicht verstehst, was mit dir geschieht, alles ist ein einziges Drunter und Drüber und Gefühle nehmen dein Leben unerwartet ein.
Dann ist das Leben genau so, wie es sein sollte. Dann ist es so, dass wir wirklich leben und in jeder Zelle spüren, dass wir da sind. Dann ist das Gefühl, das wir von uns selbst haben, deutlich und stark in all dem Chaos und all der Undeutlichkeit. Dann merken wir den Wert des Daseins. Das ist nicht, was wir uns verdienen oder ver­masseln können. Es ist unwillkürlich da, dadurch, dass es uns gibt.
Dieser Zustand, diese Augenblicke sind oft kurz und intensiv, aber die Erinnerung an sie vergoldet das Leben und gibt uns Möglichkeiten für völlig andere Perspektiven, als wenn wir wählen würden, wie wir im Sekundären leben wollten, in welcher Familienform, Wohnform, Versorgungsform, usw.
Das Leben ist eine harte Arbeit, ein unbeqemes und mühseliges Ungemach, aber all das ist notwendig für diese aufglimmenden Augenblicke von Glück. Sie folgen nicht automatisch einer harten Arbeit, aber sie werden möglich, wenn wir uns selber ”in Anspruch” nehmen, wenn wir in Übereinstimmung mit unserer Natur und unserem Charakter leben.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Unter welchen Verhältnissen lebst du?
Woran passt du dich an?
Wie denkst du über Geld, Arbeit, Wohnen, Auto usw?
Gibt es etwas in deinem Leben, was befreit ist von der Ökonomisierung?
Gibt es etwas, was du verändern möchtest, um eine bessere Lebensqualität zu bekommen? (oft für den Preis deiner Bequem­lichkeit.)

</div>
# Einige Worte über verlorene Autonomie und verlorene mutualistische Gemeinschaft

Wir bilden uns oft ein, dass Liebe ein Gefühl ist, welches wir von anderen bekommen können, etwas, was wir uns durch unser Bemühen verdienen können und was wir entgegen nehmen oder von uns weisen können, je nachdem wie wir meinen, dass es der andere verdient hat.
So können wir es hinkriegen, glauben wir, wie eine sich selbst erfüllende Prophezeiung. Aber wenn wir wirklich erkennen, was Liebe ist, dass Liebe ein Zustand ist, der immerzu da ist, in uns und um uns herum, den wir in unser Fühlen hochholen können, wann immer wir wol­len, dadurch, dass wir uns gegenüber dem Leben so verhalten, dass das „Wie“ und nicht das „Was“ unseres Denkens und Handelns wichtig wird - dann wird sie etwas ganz anderes.
Die Fähigkeit, Liebe in ein Gefühl zu heben, hängt mit unserer Fähigkeit zu Autonomie zusammen. Wenn wir eine äußere Geborgenheit während unserer Zeit des Heranwachsens erlebt haben, eine Geborgenheit, die zuließ, dass wir in uns verbleiben konnten und uns reflektieren, entdecken und die Welt erforschen ließ aus unseren eigenen Gefühlen und Sinnen heraus, dann wird es hell in uns drin und wir haben es einfach damit, Liebe in ein Gefühl hochzuheben und sie für uns selbst und andere zu fühlen.
Es hängt auch davon ab, ob wir während der ersten zehn Jahre Menschen um uns gehabt haben, die Zeit für uns hatten, die uns zuhörten und unsere Fragen beant­wor­teten. Ob es immer jemanden gab, an den wir uns wenden konnten, wenn wir es nötig hatten, jemand, der uns ein wenig von seiner Lebenserfahrung hatte vermitteln können. Gerade die Möglichkeit zu einer mutualistischen Beziehung.
Es hängt also davon ab, ob die Menschen, die um uns herum waren, Freude und Zufriedenheit erlebten im Umgang mit Kindern. Ob sie offene und neugierige Sinne hatten für die Persönlichkeit jedes einzelnen Kindes und dessen spezieller Entwicklung. Ob sie es sein lassen konnten, das Kind nach ihren eigenen Vorstellungen zu formen, sondern sich als Begleiter erlebt haben, der unterstützte und Gefahren abwehrte.
Was die Autonomie bei einem Kind außer Kraft setzt, ist oft, dass die Eltern gestresst sind - dass sie zur Arbeit müssen, die Kinder zur Tagesschule bringen, einkaufen, putzen usw... Diese sekundären Tätigkeiten können gut und notwendig sein, aber dort walten ganz andere Ver­hältnisse als in den primären Tätigkeiten.
Die Kinder fühlen sich einsam und unerwünscht, wenn die Eltern so viel von ihrer Aufmerksamkeit auf die sekundären Tätigkeiten richten. Sie kommen gestresst von der Arbeit und holen schnell ihr Kind von der Tagesschule. Dann muss alle notwendige Arbeit schnell und effektiv getan werden, damit man eine Weile ausspannen kann, am besten ohne Einmischen der Kinder, denn das braucht mehr Zeit und ist umständlicher. Erst wenn es Zeit fürs Schlafengehen ist, gibt es ein wenig Platz für Intimität, wenn jemand eine kleine Erzählung für das Kind vorliest, aber leider lernen die Kinder sehr früh lesen und dann müssen sie selber lesen.
Am Wochenende wird all das getan, wozu man unter der Woche keine Zeit gefunden hat. Man muss auch Freunde besuchen und manchmal etwas unternehmen, das Spass macht. Das ist Tun, Tun, Tun die ganze Zeit, mit einem Tempo, das von einem Gefühl gesteuert wird, dass man alles, was man tun will, in einen viel zu kleinen Raum stopfen muss. Dann kommen die Ferien oder Feiertage. Da versuchen die Erwachsenen, ihr schlech­tes Gewissen zu kompensieren, merken jedoch, dass sie es nicht schaffen. Nun wollen sie nur in Ruhe gelassen werden und abschalten, um ”die Batterien wieder auf­zuladen”, damit sie mit der Arbeit für den Rest des Jahres zurechtkommen.
Dann fängt das Kind in der Schule an. Dort sind die Bedingungen so, dass man das leistet, was der Lehrer von einem erwartet und man die Tauglichkeit des Lehrers bestätigt dadurch, dass man gute Noten bekommt. Diese Erwartung der Anpassung fängt schon in der Tagesschule und dem Kindergarten an, dauert an, bis man aus dem Gymnasium kommt und da ist man schon 20 Jahre alt, für viele kommen noch vier Jahre Hochschule dazu.
Unsere ganze Gesellschaft baut auf Anpassung. Wir werden aufgefordert, etwas oder jemand zu werden, Status ist wichtig, wir müssen langfristige Ziele haben und etwas leisten, was sich sehen lässt, so dass wir uns stolz fühlen und bewundert werden können. Wir sollten danach streben, viel Geld zu verdienen, teuere und bequeme Wohnstätten zu bauen, viele Interessen zu haben, alle möglichen Sportarten auszuüben, am besten solche, die viel Geld kosten, denn dann begünstigen wir außerdem die Wachstumswirtschaft.
Das Primäre wird so wenig wertgeschätzt, beinahe gar nicht. Nur in sich selbst sein, autonom und mit anderen, mutualistisch. Fühlen, dass man alle Zeit der Welt hat, nur da sein und ein Gefühl von äußerer Sicherheit ge­nießen, wenn man weiß, dass der Hunger nicht vor der Türe steht, dass wir ein Dach über dem Kopf haben und Wärme, damit wir geschützt schlafen können. Diese Qualität ist unbezahlbar und kann nicht genug gepriesen werden. Wenn wir ein solches Gefühl erleben können, wird es hell in unserem Gemüt und voll Freude in unserem Inneren und wir bekommen Zugang zum Lie­bes­zustand, den wir dann in unser Fühlen heben können.
Sicherlich kann das mit sich führen, dass wir nicht in einem eigenen Haus wohnen können und dass wir nicht das teuerste Auto haben können, den besten Fern­seh­apparat, ein Übermaß an Markenkleidern usw., aber als Entgeltung haben wir es leichter, die Freude an all den unvorhergesehenen Ereignissen des natürlichen Lebens zu erleben.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wenn du dir überlegst, was du eigentlich brauchst, um ein gutes Leben zu führen und wieviel von dem, womit du dich umgibst, Luxus und Überfluss ist, und dann darüber nachdenkst, wieviel Zeit von deinem Leben du zu geben gezwungen bist, um all das haben zu können, ohne das du sehr gut zurecht kämst, und darüber reflektierst, wovon diese Zeit genommen wird, wie das deine Lebensqualität beeinflusst, bist du dann zufrieden mit der Weise, wie du lebst?
Wenn du darüber reflektierst, woran du dich anpasst, was alle diese Dinge anbelangt, wes­sen Erwartungen du Genugtuung leistest, wel­chen Preis du bezahlst und welchen Lohn du bekommst für deine Mühe, bist du da zufrieden mit deiner Wahl?
Wenn du merkst, dass du ein Opfer der Anpassung bist, und dass du symbiotisch Dinge tust, weil andere das erwarten, oder du merkst, dass du das Entgegengesetzte tust, dass du trotzt und dich weigerst, das zu tun, was andere erwarten, wie denkst du da? Willst du weiter auf diese Weise leben, oder kannst du dir vorstellen, darüber nachzudenken, wie du dich jeder einzelnen Sache gegenüber verhältst und sehen, ob du sie zu deiner eigenen umwandeln kannst und damit zufrieden sein kannst, oder musst du vielleicht deine ganze Lebensweise än­dern?

</div>

Dieses Heft richtet den Fokus nur auf einen kleinen Teil eines sehr umfangreichen Gebietes des Lebens. Es ist in erster Linie gedacht als eine Hilfe bei der Begegnung mit verschiedenen Phänomenen, in welche wir oft un­re­flek­tiert involviert werden. Es gibt uns eine Chance, neue Möglichkeiten zu sehen und eventuell das zu ändern, von dem wir wissen, dass es uns irritieren wird oder worüber wir unzufrieden sein werden, wenn wir im Sterbebett liegen.

Iris Johansson

Wenn Sie eine Konsultation wünschen, rufen Sie bitte die Nummer +46 708 878688 an oder mailen Sie an: iris@irisjohansson.com.
