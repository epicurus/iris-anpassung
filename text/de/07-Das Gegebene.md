## Das Gegebene

Gemeinschaft ist das Natürliche für uns. Sie wird jedoch oft aus der Bahn geworfen aufgrund der Ängste vor den Konsequenzen, sowohl praktisch als gefühlsmäßig. Deshalb wollen wir Gemeinschaft schaffen, aber das geht nicht. Gemeinschaft kann man nicht schaffen, die gibt es, wenn wir nur das wegnehmen, was ihr im Wege steht. Das ist es, was sich so schwierig ausnimmt. Zum Teil zu sehen, was der Gemeinschaft im Wege steht, aber auch in wirklicher Gemeinschaft zu stehen, wenn man alle Hin­der­nisse entfernt hat. Das ist oft schmerzlich und wir fürchten uns oft vor dem Schmerz, der der Intimität vorausgeht. Oft umgehen wir die Intimität und gehen über in eine Sexualisierung, werden davon angezogen und verschieben den Fokus auf das Geschlechtliche, anstelle von menschlicher Begegnung.
Es ist nichts falsch an Verliebtheit und Sexualität, aber wenn sie eine Art der Flucht vor Intimität sind, dann werden sie leer und sinnlos. Es wird kränkend und abwertend, sowohl für einen selbst, als auch für den anderen. Das Resultat ist, dass wir leicht in einem symbio­tischen Verhältnis landen, wo wir uns in die gegenseitigen Gefühle und Gedanken verwickeln. Wir geraten in ein Gefühlsspiel anstelle von Intimität und werden leicht eifersüchtig, flehend und beherrschend. Dies ist auch ein Ausdruck für die Schadenswirkungen der Anpassung.
Viele verschiedene Aspekte der Gefühlsspiele, in die wir oft geraten, wenn wir eigentlich Intimität erleben sollten, sind ausgiebig beschrieben in einem Buch von Erik Berne, das ”Games People Play” heißt.
Die Spiele haben oft einen treffenden Titel, der macht, dass wir leicht verschiedene Spiele, in denen wir mitten drin stecken, entlarven können. Manchmal sind wir es selbst, die mit den Spielen anfangen, manchmal fallen wir in diejenigen anderer hinein. In beiden Fällen müssen wir beachten, dass es an Intimität mangelt und dass dies die Hauptursache dafür ist, dass wir uns verstricken.
”Weshalb tust du nicht..?.“
“Ja aber...”
Stellen wir uns vor, wir würden jemanden treffen, die gerne Hilfe und Unterstützung haben möchte, jemanden, der uns um Rat fragt. Wir fühlen uns geehrt und strengen uns an, richtig gute Ratschläge zu geben. Aber alle Ratschläge werden zurückgewiesen mit einer Er­klärung dafür, weshalb es nicht geht, sie auszuführen. Anfänglich hören wir mit Verständnis zu, doch nach einer Weile fangen wir an, leicht irritiert zu werden. Was so gut anfing damit, dass wir in Anspruch genommen wurden und Vertrauen erhielten, endet damit, dass wir uns als Opfer erleben und uns machtlos fühlen, weil nichts von dem, womit wir beitragen, taugt.
Das Resultat wird, dass beide sich enttäuscht fühlen, derjenige, der um Rat gebeten hat, fühlt, dass es sowieso vorbei ist, dass es nicht geht, etwas aus der Situation zu machen, und derjenige, der so bereitwillig Ratschläge ausgeteilt hat, fühlt sich ausgenützt und missbraucht.
Als Folge wird sich jeder im Wechselspiel der Rollen des Retters, Täters oder Opfers wiedererkennen und darin sein wankendes Selbstwertgefühl und seine bisherigen Werte vom Leben und den Menschen bestätigt fühlen.
“Ich muss nur kurz...”
Jedesmal, wenn man die Gelegenheit hat, etwas Roman­tisches, Freundliches, Spaßiges, Genussvolles zu unter­nehmen, kommt der andere auf etwas, was unbedingt zuerst erledigt werden muss. Derjenige, der eingeladen hatte, etwas zu unternehmen, wartet und wartet und ..., bis er oder sie dermaßen irritiert und frustriert ist, dass dasjenige, was so reizend war, verschwindet und deshalb egal ist. Beide werden die Situation als misslungen emp­finden und werden eine Spur von negativer Bestätigung in der Enttäuschung erleben.
Erwachsene tun oft so mit Kindern, um der Intimität mit dem Kind aus dem Wege zu gehen. Das führt dazu, dass das Kind sich nicht okay fühlt, fühlt, dass etwas an ihm nicht stimmt, und so fängt es an, sich anzupassen, um nicht zu sehr „daneben“ zu sein.
Aber es hilft doch nicht, weil der Erwachsene trotzdem immer intuitiv einen Grund finden wird, der Intimität auszuweichen.
”Schau, wozu du mich gebracht hast!”
Wenn einem etwas missglückt ist oder etwas geschieht, was nicht berechnet war, etwas, was kritisiert werden kann, dann meint man oft, jemand anderes habe geschal­tet und gewaltet, der andere sei an dem Ganzen schuld. Dieses Spiel kann auch sein ”wenn du nicht wärest, dann...”
Wenn man es selbst nicht hinkriegt, in der Intimität zu verbleiben, die entsteht, wenn einem etwas misslingt oder man einen Fehler begeht, wenn man nicht in dem Schmerz verweilen kann und die Konsequenzen und die Verantwortung für die Situation auf sich nehmen kann, dann wird man ein Opfer für seine eigenen Fehlschritte werden. Das wird so unerträglich, dass man ein Gefühls­spiel startet, das andere mit in den eigenen Konflikt zieht, nur um von der Intimität fortzukommen.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Kennst du etwas davon wieder?
Wenn ja, wie denkst du dann über Intimität, anstelle von Gefühlsspiel?
Kannst du irgendeine Möglichkeit sehen, dies zu durchbrechen und in eine Begegnung mit demjenigen zu treten, mit dem du im Gefühlsspiel bist oder der mit dir im Ge­fühls­spiel ist?
Kannst du darauf pfeifen, Recht zu haben?
Kannst du auf Rache und Revanche pfeifen?

</div>
