# Zutrauen zu mir selbst

Ein anderer Zustand als Anpassung findet sich ein, wenn du in dich selbst gehst und die Freude findest. Dann entstehen ganz andere Gedanken.
Dann fühlst du dich frei und zufrieden, erhältst eine Menge Energie und fühlst, dass du alle Kraft der Welt hast. Dann gibt es die innere Geborgenheit, die macht, dass das Ursprüngliche zugänglich wird, Phantasie, Krea­tivität, Neugierde, Lust und vieles mehr. Das nenne ich Autonomie, ein Zustand, wo man sich selbst zufrieden stellt und wo das Leben genau so ist, wie es sein soll.
Wenn du jemals im Leben so gefühlt hast, dann weißt du, dass es so etwas gibt. Es ist da, wenn nichts anderes da ist und dabei ist es ein Anliegen, etwas mit dem zu tun, was im Weg steht, so dass man dann und wann einen Lichtstrahl davon erhält.
Wenn du niemals so gefühlt hast, wenn du nicht verstehst, wovon ich spreche, dann ist es höchste Zeit, dass du Hilfe dabei bekommst, eine Wahrnehmung von dem zu er­halten, was waltet, wenn nichts anderes waltet.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Setze dich bequem hin und sei da anwesend, wo du gerade bist.
Konstatiere, dass du in Sicherheit bist, dass nichts da ist, das dein Leben bedroht.
Falls etwas auftaucht, das sich in den Weg stellt, schreibe dann auf, welche Gedanken und Gefühle auftauchen und dich be­hin­dern.
Denke: ”Der Sinn des Lebens ist zu leben und ich lebe, ich bin da - hier und jetzt bin ich genau so, wie ich bin”.
Wenn das nicht geht, schreibe auf, was dich hindert.

</div>
