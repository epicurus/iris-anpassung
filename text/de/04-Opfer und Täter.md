# Opfer und Täter

Für viele ist es nicht einmal möglich, überhaupt still zu sitzen und sich dafür Zeit zu nehmen. Sie bekommen Angst, es wird zu schmerzlich, es fühlt sich an, als wenn sie sterben müssten oder verrückt würden, falls sie eine solche Übung machen würden. Für einige fühlt es sich nur lächerlich an und abgeschmackt und ”gibt nichts her“. Für andere ist es schockartig traumatisch und alle un­heim­lichen Erinnerungen halten Einzug in sie.
Alle, die eine innere Zufriedenheit erleben können wäh­rend einer solchen Übung, die können leicht mit Hilfe von primärer Gedankenarbeit jenes innere Gleichgewicht erlangen, das macht, dass das Leben so wird, wie es sein soll, voller Kraft und Energie, Engagement und allen nur erdenklichen Gefühlen.
Andere können Hilfe nötig haben, um mit einer Pri­märarbeit in Gang zu kommen, dadurch, dass jemand anderes sie dahin führt, so dass sie Kontakt bekommen mit dieser Dimension der Anwesenheit und Lebenskraft, die ja immerzu da ist.
Für diejenigen, die nicht einmal in die Nähe von diesem kommen können, die die ganze Zeit mit einer Menge von Bewertungen kämpfen und inneren Vorstellungen davon, nicht zu taugen, nicht das Recht zu leben zu haben, nicht glücklich zu sein, nicht spielen oder fröhlich sein zu dürfen, nicht erwachsen und nicht klein, die sind zu tief in der Anpassung stecken geblieben. Sie ahnen, dass sie einen weiten Weg mit harter Arbeit vor sich haben, um all das, was ihnen im Weg steht, wegzuschieben, alles das, was an Diktate angepasst ist, die oftmals vor dem Erwerb der Sprache, unverbal, sich der Persönlichkeit eingeprägt haben. Deshalb wählen diese meistens, in eine andere Richtung zu schauen.
Beziehungen zu anderen Menschen zu haben, ist das Wichtigste für unser Überleben (außer den Dingen, die man für seine Existenz braucht.)
Wir brauchen Intimität, Begegnung und Gemein­schaft. Dazu haben wir jederzeit die Möglichkeit, wenn wir einen anderen Menschen in unserer Nähe haben.
Wir wissen, wie es ist, in Begegnung zu sein. Das wird schon in der Gebärmutter angelegt, in der Beziehung von Mutter und Kind und dann nach der Geburt, in Beziehung zu den Nächsten, die die ganze Zeit um das Kind herum sind.
Wenn etwas geschehen ist, was dieses frühe Erlebnis von ”in Begegnung” zu sein aus der Bahn geworfen hat oder gestört hat, dann wird unsere Erinnerung der Intimität unzureichend sein, und dann werden wir es mit etwas anderem, z.B. mit Bewunderung, ersetzen.
Wir passen uns an andere an und werden tüchtig, stark, schnell, schmeichlerisch usw. Nicht, weil wir es sind, sondern damit andere uns gerne mögen und uns die Aufmerksamkeit und Bestätigung geben, die uns spüren lässt, dass wir jemand sind.
Das macht uns abhängig von anderen. Wir fangen an, Rollen anzunehmen, wir schaffen uns eine Identität, die uns dazu veranlasst, andere dazu zu bringen, uns zu geben, was wir brauchen.
Wenn es nicht klappt mit Bewunderung, dann können wir Macht ausüben. Wir werden unvoraussehbar, dro­hend, fordernd, abwertend, wir sehen zu, dass wir Informationen haben, die andere von uns abhängig machen, und wir streben danach, eine hierarchische Position zu bekommen, die andere dazu bringt, dass sie uns aufsuchen müssen und eine Beziehung zu uns haben müssen, damit ihr Leben funktioniert.
Wenn wir weder Bewunderung noch Macht bekommen können, können wir so beschwerlich und so pochend und fordernd sein, dass uns mit Verachtung begegnet wird.
Es gibt eine Anzahl von Gefühlsspielereien, die wir spielen können, in denen wir in verschiedene Rollenfiguren hüpfen und dadurch eine Menge Auf­merk­sam­keit und Gefühle erhalten, die machen, dass wir nicht untergehen, auch wenn wir keine positive Intimität hinkriegen.
Niemand überlebt ohne Aufmerksamkeit, auf die eine oder andere Weise sehen wir zu, dass wir sie erhalten.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Gibt es Gelegenheiten, wo du dich als ein Opfer erlebst und dich nicht unabhängig fühlen kannst von dem, was andere sagen oder tun?
Gibt es Gelegenheiten, wo du dich als Täter fühlst, wo du die Wahrheit sagen musst, kritisieren musst und anderen misstraust und dich berechtigt fühlst, auf andere los­zugehen?
Gibt es Gelegenheiten, wo du der Fähigkeit von anderen, ihre eigenen Probleme zu lösen, misstraust, so dass du eingreifst und die Person und die Situation rettest, wo du die Initiative ergreifst und eine Bedeutung be­kommst?
Gibt es Gelegenheiten, wo du in Beziehungen involviert bist und merkst, dass du in eine dieser Positionen (Opfer, Täter oder Retter) fällst, wo auch deine Position wechselt, und auch ein­ge­misch­te Personen ihre Position wech­seln, was zu einem Gefühlsspiel ausartet?

</div>
