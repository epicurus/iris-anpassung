# Machtlosigkeit

Wenn man ein Kind ist und die Erwachsenen nicht autonom, sondern angepasst sind, dann wird man in das Gefühlsspiel der Erwachsenen mit hineingezogen. Man wird zur Spielfigur. Das Kind wird dann die Bedürfnisse des Erwachsenen nach Intimität auslösen und das be­wirkt, dass die natürlichen Bedürfnisse des Kindes nach Intimität und Hilfe zu einer Konkurrenz gegenüber den Bedürfnissen des Erwachsenen werden. Dann wird der Erwachsene das Kind als anstrengend, fordernd, als böse, als irritierend usw empfinden. Er wird es abweisen, es erziehen dadurch, dass er es schreien lässt, und machtlos dem Kinde das Notwendigste geben, jedoch ohne Intimität. Es geht vielleicht sogar so weit, dass der Erwachsene das Kind schlägt, um es zu unterdrücken und anzupassen, so dass es nicht so viel Aufmerksamkeit fordert.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wenn du Kinder hast, hast du dich jemals gegenüber den Bedürfnissen des Kindes ohn­­mächtig gefühlt?
Als du selbst Kind warst, kannst du dich erinnern, wie du die Reaktionen der Er­wachsenen auf dich erlebtest?
Als du Jugendliche/r warst, hattest du da Probleme mit deinem Selbstgefühl? Zeigte es sich in Form von z. B. Esstörungen, Leis­tungszwang und Schwänzen, oder frühem sexuellem Debüt, Drogen usw.?
Hast du dich einsam, wertlos und “außer­halb“ gefühlt? Hast an deinem eigenen Wert zu existieren gezweifelt?

</div>
