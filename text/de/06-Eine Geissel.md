# Eine Geissel

Anpassung hat so viele Gesichter, dass es schwer ist, alle Konsequenzen aufzuzählen, die sie mit sich bringen kann. Aber, was wir erleben, wenn wir angepasst sind, ist ein Gefühl von Abhängigkeit von etwas oder jemandem außerhalb von uns selbst, dass wir machtlos sind und dass Verantwortung schwer zu tragen ist und nur zu Unfreiheit führt. Oft taucht ein Gefühl auf, dass man revoltieren möchte, sich herausschlagen, frei werden, Grenzen spreng­en, doch es strandet oft in einem lahmen Protest, oft darin, dass man krank wird oder es einem übel geht.
Hinter der Anpassung liegt Misstrauen in unsere eigene Fähigkeit, unsere eigenen Bedürfnisse zufriedenstellen zu können. Wir meinen, abhängig und angepasst sein zu müssen, um das zu bekommen, was wir für unser Überleben brauchen. Wir werden gefügig und gesell­schaftlich angepasst.
Hinter der Anpassung liegt auch Furcht davor, „außer­halb“ zu sein, nicht mit dabei zu sein, abgewiesen zu werden und in Konflikten zu landen, die uns ausstoßen und einsam machen. Es wird auch schwierig, zu wählen. Alles, was nicht von Anfang an kontrollierbar ist, weckt schon im Voraus Angst.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

In Bezug auf andere Menschen fürchtest dich davor, ...
... abgewiesen zu werden.
... mit anderen in Konflikt zu geraten.
... Verantwortung auf dich zu nehmen und Kritik für die Konsequenzen zu bekommen.
... nicht genug tüchtig zu sein oder zu tüchtig zu sein.
... dass andere von dir abhängig werden.
... dass jemand einen Haken bei dir findet, so dass du ”entlarvt”werden kannst.
... dass du in einer Abhängigkeit landest.

</div>
