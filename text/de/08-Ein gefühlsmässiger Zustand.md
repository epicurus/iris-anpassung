# Ein gefühlsmässiger Zustand

Spüre nach und mach dir Gedanken über „Begegnung“, „Intimität“, „Anwesenheit“, „Gemeinschaft“, „offen und verletzlich sein“, „Schmerz“ und „Zufriedenheit“. Fühle nach, wie all das zusammenhängt, wie es un­voraussehbar ist und dass es positiv angsterfüllt sein kann. Fühle, wie die Freude in dir aufsprudelt und sich manch­mal wie ein unausstehbarer Schmerz anfühlen kann, fühle, wie du eine innere Geborgenheit bekommst, die bewirkt, dass ein Signal dir anzeigt, dass du ”ausladen” kannst, dass alte gelagerte Gefühle und Erinnerungen hochkommen und machen, dass du weinst und schreist, rasend bist und frierst in einem wilden Durcheinander. Du kennst dich selbst nicht mehr wieder, oder du erlebst es, wie wenn du nicht verstehst, was mit dir geschieht, alles ist ein einziges Drunter und Drüber und Gefühle nehmen dein Leben unerwartet ein.
Dann ist das Leben genau so, wie es sein sollte. Dann ist es so, dass wir wirklich leben und in jeder Zelle spüren, dass wir da sind. Dann ist das Gefühl, das wir von uns selbst haben, deutlich und stark in all dem Chaos und all der Undeutlichkeit. Dann merken wir den Wert des Daseins. Das ist nicht, was wir uns verdienen oder ver­masseln können. Es ist unwillkürlich da, dadurch, dass es uns gibt.
Dieser Zustand, diese Augenblicke sind oft kurz und intensiv, aber die Erinnerung an sie vergoldet das Leben und gibt uns Möglichkeiten für völlig andere Perspektiven, als wenn wir wählen würden, wie wir im Sekundären leben wollten, in welcher Familienform, Wohnform, Versorgungsform, usw.
Das Leben ist eine harte Arbeit, ein unbeqemes und mühseliges Ungemach, aber all das ist notwendig für diese aufglimmenden Augenblicke von Glück. Sie folgen nicht automatisch einer harten Arbeit, aber sie werden möglich, wenn wir uns selber ”in Anspruch” nehmen, wenn wir in Übereinstimmung mit unserer Natur und unserem Charakter leben.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Unter welchen Verhältnissen lebst du?
Woran passt du dich an?
Wie denkst du über Geld, Arbeit, Wohnen, Auto usw?
Gibt es etwas in deinem Leben, was befreit ist von der Ökonomisierung?
Gibt es etwas, was du verändern möchtest, um eine bessere Lebensqualität zu bekommen? (oft für den Preis deiner Bequem­lichkeit.)

</div>
