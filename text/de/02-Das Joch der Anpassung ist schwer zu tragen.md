## Das Joch der Anpassung ist schwer zu tragen

Anpassung – GESPRÄCH

Was ist das, dass so viele Menschen fühlen, dass es ihnen nicht gut geht, dass es ihnen schlecht geht, obwohl sie alles haben, was sie brauchen für ihren Lebensunterhalt, und mehr dazu? Sie haben jeden Tag zu essen, ja so viel, dass sie ständig gezwungen sind, einiges wegzuwählen, und sie haben so viele Kleider, dass sie gezwungen sind, aus der jeweiligen Situation, Lust und Gutdünken heraus zu wählen. Sie haben nicht nur ein Dach über dem Kopf, sondern außerdem teure Wohnhäuser, die sie in eine Lage versetzen, in der sie viel zu viel von ihrer Zeit hergeben müssen, nur allein fürs Wohnen.
Was ist es, das macht, dass wir uns oft in eine Situation versetzen, wo wir so viel Geld verdienen ”müssen” und wir gleichzeitig finden, dass wir dabei an Lebensqualität verlieren?
Das Joch der Anpassung ist schwer zu tragen.
In unserer Kultur wird Anpassung als eine Tugend an­gesehen. Es ist fein, sich anzupassen. Man erhält einen hohen Status, wenn man tüchtig, schnell und effektiv ist und tut, was andere sagen.
Als Erwachsener bedeutet es Status, bequem zu sein, es hin­zukriegen, dass andere für einen arbeiten, auf irgendeine Weise der Leiter zu sein und andere unter sich zu haben, die ”die Arbeit machen”, auch alle Bequemlichkeiten wie Auto, Spülmaschine usw. zu haben.
Kinder an Tagesschulen werden gerühmt, wenn sie ”lieb” sind, d.h. sich an das anpassen, was der Erwachsene will, so dass der Erwachsene die Mühe umgehen kann, den Fokus zu verschieben und zusammen mit dem Kind zu reflektieren darüber, was vernünftig wäre und Freude bereiten würde. Jene Kinder, die dadurch protestieren, dass sie trotzen, werden als „schwierig“ angesehen und da wird es für den Erwachsenen wichtig ”die Macht an sich zu reißen, laut und deutlich zu werden und Grenzen zu setzen”. Trotzige Kinder passen sich nur durch Zwang an.
In der Schule wird von den Kindern erwartet, dass sie still sitzen und ruhig sind und lernen, was der Lehrer will, damit der Lehrer sich tüchtig fühlen kann, d.h. Beweise für seine Tauglichkeit durch das Resultat der Kinder erhält. Schüler, die es nicht mehr schaffen sich anzupassen, schwänzen und machen Radau und damit konstatiert die Erwachsenenwelt, dass etwas beim Individuum nicht stimmt, d.h. bei dem Schüler, oder dass er oder sie auf dem Weg in die Pubertät ist.
Zu Hause ist es dieselbe Sache. Die Eltern wollen, dass die Kinder sich gut benehmen und tüchtig sind, so dass sie in Ruhe arbeiten und sich selbst verwirklichen können, und nur dann Freude an ihren Kindern haben dürfen und in Gesellschaft anderer Stolz auf sie sein können. D.h. die Kinder räumen ihre Zimmer auf, putzen, machen die Hausaufgaben, betreiben Sport und sind fröhlich.
Das führt dazu, dass wir aus einer Bewertung heraus leben, die besagt, dass wir nur taugen, wenn wir perfekt, tüchtig, schnell, effektiv, reich sind und von anderen wegen unserer Vortrefflichkeit bewundert werden. In der Schule ist der Beweis dafür ein gutes Zeugnis. Die Kriterien im Freundeskreis sind: ”in” zu sein, das ”rechte” Aussehen zu haben, coole Kleider zu tragen und mit einer Menge Abwertungen umgehen zu können, wie z.B. „Hure“, „Schwuler“ und anderes.
Sich an die Bewertungen von anderen anzupassen, führt zu Unfreiheit und das mögen wir überhaupt gar nicht. Wir werden krank an Körper und Seele und es geht uns schlecht. Lange Zeit versuchen wir, das Problem dadurch zu lösen, dass wir noch mehr von dem verrichten, was uns schadet, wie etwa uns zur Verfügung zu stellen, uns aufzuopfern, bis zur Selbstaufgabe, generös zu sein, usw.
Bei Jugendlichen, die es nicht schaffen, sich anzupassen, führt das oft dazu, dass sie sich von der Erwachsenenwelt distanzieren und eine Subkultur entwickeln, wo sie ver­suchen, eigene Alternativen zu finden. Sie zeigen ihren Protest, indem sie Drogen nehmen, vandalisieren, kriminell sind, Esstörungen entwickeln oder Trainingsfreaks werden usw.
Bei einem Erwachsenen führt das dazu, dass er ”gegen eine Wand rennt”, ausgebrannt wird, Fibromyalgie (Faser-Muskel-Schmerz) be­kommt, verschiedene Allergien oder andere Über­emp­findlichkeiten. Viele werden von psychischen Problemen befallen und einige versuchen, sich das Leben zu nehmen.
Der Mythos in unserer Zeit ist, dass alles, was geschieht, individuell bedingt ist, dass es auf unserer Konstitution beruht oder auf dem, was wir als Kind erlebt haben. Es ist richtig, dass man immer individuelle Ursachen finden kann dafür, dass ein Mensch leidet, aber es stimmt nicht, dass 60% der in Luxus aufgewachsenen Menschen geschä­digt sind. Höchstens 3% haben eigene individuelle Pro­bleme, die bewirken, dass sie solche Symptome haben. Die übrigen sind Opfer von Denkfehlern.
Das große Problem ist, dass viele Menschen nicht wissen, dass es etwas anderes gibt; eine andere Lage, eine andere Verhaltensweise, als die angepasste. Dass es etwas anderes gibt als das, was gegeben ist: entweder sich in die Dinge zu fügen oder gegen das zu trotzen, in das sich andere fügen. Das sind zwei Seiten der Anpassung und sie haben nichts gemeinsam mit einer gesunden, frei auto­nomen und geborgenen Art zu funktionieren.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />
Erkennst du darin etwas aus deinem Leben wieder?
Gibt es etwas, was dich daran hindert, du selbst zu sein?
Weißt du, wer du bist?
Wovon gehst du aus, wenn du an den Sinn des Lebens denkst?
Wie denkst du über deinen eigenen Wert und den von anderen?
</div>

Autonomie

Im Grunde gibt es keine Bewertungen oder Bedingungen, die uns steuern, sondern das, was steuert, ist der Wunsch zu überleben, der Wille zu existieren. Es ist ein Kampf, sich zu entwickeln schon in der Gebärmutter, ein Kampf gegen alle zersetzenden Kräfte - dass wir heute da sind, zeigt, dass es uns gelungen ist, alle Gefahren zu über­winden.
Wir sind neugierig, aktiv handelnd, kreativ und schöpferisch. Wir sind erforschend und gleichzeitig völlig hilflos - wir sind abhängig von einer Umgebung, die sich um uns kümmert. Ein kleines Kind kann anfangs nicht einmal selbst essen, sondern braucht, dass wir einen Saugreflex bei ihm hervorlocken.
Wenn ein Kind in einer Umgebung aufwächst, in der es zufriedene und fähige Menschen gibt, die sich ganz selbstverständlich um das Kind kümmern, ohne das Bedürfnis zu haben ”tüchtig zu sein” und Bestätigungen dafür zu bekommen, dann hat das Kind eine große Chance, frei zu sein.
Paradoxerweise ist das Kind völlig abhängig von seiner Umgebung, um überleben zu können und deshalb ist es für den Erwachsenen so verlockend, dieses Ab­hängig­keitsverhältnis auszunutzen, um sich selber als tüchtig zu erleben. Aber gleichzeitig ist das Kind fähig, vollkommen autonom, ganz auf seine eigene Weise zufrieden zu wer­den mit dem, was es bekommt, dadurch, dass es den Fokus auf sich selbst richtet und sich zufrieden fühlt, wenn die Grundbedürfnisse gedeckt werden.
Es gibt eine Vorstellung über uns Menschen, die mit der Wirklichkeit nicht übereinstimmt, die uns jedoch über lange Zeit geprägt hat. Diese, dass wir, falls wir nicht lieb gemacht werden, böse und gefährlich werden. Das kommt daher, dass wir ”arme Sünder sind, die im Schweiße ihres Angesichts ihr Brot verdienen müssen”. Das sind Vorstellungen, die in der Entwicklung der Menschheit lange Zeit mitgeschleift wurden und ihre Wurzeln in den Deutungen alter biblischer Schriften haben. Solche Ansichten über den Menschen haben zur Folge, dass wir der Auffassung sind, ein Kind in die Anpassung drücken zu müssen, damit es fromm wird.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wie steht es mit dir und deiner Auffassung über den Menschen, glaubst du, dass wir im Grunde böse sind und zum Guten erzogen werden müssen?
Oder glaubst du, dass wir im Grunde gut sind und deshalb keine Stopps und Grenzen brauchen?
Oder glaubst du, dass wir weder gut noch böse sind, sondern das eine oder das andere werden auf Grund von dem, was uns zu­stößt?
Wie denkst du über dich selbst, erlebst du dich als geprägt von der Erziehung, der du ausgesetzt warst?
Wenn du ein Kind hast und dieses Kind weigert sich, sich an das anzupassen, was du willst, wie kommst du damit zurecht?
Nimm dir einige Minuten und denke nach, mach gerne eine Runde und frage, was die anderen sagen, überdenke, wie du selber reagierst. Oft spiegelt das, welchen Dingen du selbst ausgesetzt warst.

</div>
